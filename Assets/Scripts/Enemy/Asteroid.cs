using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace MeatLab
{
	public class Asteroid : Enemy
	{
	
		[SerializeField] private float _maxTorgue;

		[SerializeField] private GameObject _baby;
		[SerializeField] private int _babyCount;

		[SerializeField] private Collider2D _collider; 


		protected override void Start()
		{
			base.Start();
			_collider.enabled = true; 
		}

		protected override void Move()
		{
			int rngX = Random.Range(0, 2);
			rngX = rngX == 0 ? 1 : -1;
			int rngY = Random.Range(0, 2);
			rngY = rngY == 0 ? 1 : -1;

			Vector2 thrust = new Vector2(Random.Range(_minThrust, _maxThrust) * rngX, Random.Range(_minThrust, _maxThrust) * rngY);
			float torgue = Random.Range(-_maxThrust, _maxThrust);

			_rb.AddForce(thrust);
			_rb.AddTorque(torgue);
		}

		protected override void OnTriggerEnter2D(Collider2D collision)
		{
			base.OnTriggerEnter2D(collision);
			if (collision.CompareTag("Bullet"))
			{
				collision.gameObject.SetActive(false);
				Die();

			}
			
		}

		public override void Die()
		{
			if (_isDead) return;
			_isDead = true;

			ObjectPooler.Instance.SpawnParticleSystemFromPool("Explosion", transform.position, transform.localRotation);
			ObjectPooler.Instance.SpawnFromAudioPool("SFX", Vector3.zero, _explosionSFX);
			
			_onDied?.Invoke(_score);
			for (int i = 0; i < _babyCount; i++)
			{
				Instantiate(_baby, transform.position, Quaternion.identity);
			}
			Destroy(this.gameObject);
		}
	}
}