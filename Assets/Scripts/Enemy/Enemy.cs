using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MeatLab
{
	public abstract class Enemy : MonoBehaviour
	{
		[SerializeField] protected float _maxThrust;
		[SerializeField] protected float _minThrust;

		[SerializeField] protected AudioClip _explosionSFX;

		[SerializeField] protected IntGameEvent _onDied;

		[SerializeField] protected int _score;

		[SerializeField] protected Rigidbody2D _rb;

		protected bool _isDead = false;
		protected virtual void Start()
		{
			Move();
		}

		protected virtual void Move() { }
		

		protected virtual void OnTriggerEnter2D(Collider2D collision)
		{
			if (collision.CompareTag("Jet"))
			{
				Die();
				collision.GetComponent<JetAI>().TurnOffAndOn(); 
			}
		}

		public virtual void Die()
		{
			if (_isDead) return;
			_isDead = true;
		}
	}
}

