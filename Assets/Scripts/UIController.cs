using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
namespace MeatLab
{
    [RequireComponent(typeof(GameController)), RequireComponent(typeof(SpecialAbilities))]
    public class UIController : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI _scoreText;

        [SerializeField] private Transform _heartParent;
        [SerializeField] private GameObject _heart;

        [SerializeField] private Transform _jetParent;
        [SerializeField] private GameObject _jet;

        [SerializeField] private GameController _gameController;
        [SerializeField] private SpecialAbilities _specialAbilities;

        private void Awake()
		{
			if (_gameController == null)
			{
                _gameController = GetComponent<GameController>(); 
            }
            if (_specialAbilities == null)
            {
                _specialAbilities = GetComponent<SpecialAbilities>();
            }
        }
		private void Start()
		{
            PopulateHearts();
            PopulateJets();
        }

		public void UpdateScore()
        {
            _scoreText.text = _gameController.Score.ToString();
        }
        public void PopulateHearts()
		{
			DestroyChildren(_heartParent);
			for (int i = 0; i < _gameController.Lives; i++)
			{
				Instantiate(_heart, _heartParent);
			}
		}

	

		public void PopulateJets()
        {
            DestroyChildren(_jetParent);
            for (int i = 0; i < _specialAbilities.TheBoysCount; i++)
            {
                Instantiate(_jet, _jetParent);
            }
        }

        private void DestroyChildren(Transform transform)
        {
            foreach (Transform child in transform)
            {
                GameObject.Destroy(child.gameObject);
            }
        }

    }
}