using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MeatLab
{
	public class BulletMove : MonoBehaviour
	{
		[SerializeField] private Rigidbody2D _rb;
		[SerializeField] private float _thrust;
		[SerializeField] private float _bulletLife; 
		private void OnEnable()
		{
			_rb.velocity = Vector2.zero; 
			StartCoroutine(WaitAFrame());
		}

		private IEnumerator WaitAFrame()
		{
			yield return new WaitForEndOfFrame();
			_rb.AddForce(transform.up * _thrust, ForceMode2D.Impulse);
			yield return new WaitForSeconds(_bulletLife);
			gameObject.SetActive(false);
		}
	}
}