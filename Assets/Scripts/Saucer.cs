using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace MeatLab
{
    public class Saucer : Enemy
    {
		
		enum SaucerType
		{
			BIG, 
			SMALL
		}
		[SerializeField] private SaucerType _saucerType;

		public Transform Target;
		[SerializeField] private Transform _firepoint; 


		protected override void Start()
		{
			base.Start();
			InvokeRepeating("Shoot", 2.0f, 2.0f); 
		}

		protected override void Move()
		{
			int rngX = Random.Range(0, 2);
			rngX = rngX == 0 ? 1 : -1;
			Vector2 thrust = new Vector2(Random.Range(_minThrust, _maxThrust) * rngX, 0);
			_rb.AddForce(thrust);

		}

		protected override void OnTriggerEnter2D(Collider2D collision)
		{
			base.OnTriggerEnter2D(collision); 
			if (collision.CompareTag("Bullet"))
			{
				
				collision.gameObject.SetActive(false);
				Die();
				
			}
			if (collision.GetComponent<Enemy>())
			{
				collision.GetComponent<Enemy>().Die(); 
			}
		}

		public override void Die()
		{
			base.Die();
			ObjectPooler.Instance.SpawnParticleSystemFromPool("Explosion", transform.position, transform.localRotation);
			ObjectPooler.Instance.SpawnFromAudioPool("SFX", Vector3.zero, _explosionSFX);
			_onDied?.Invoke(_score);
			Destroy(this.gameObject);
		}

		private void Shoot()
		{
			switch (_saucerType)
			{
				case SaucerType.BIG:
					float randomCircle = Random.Range(0, 360.0f);
					ObjectPooler.Instance.SpawnFromPool("EnemyBullet", transform.position, Quaternion.Euler(0, 0, randomCircle));
					break;
				case SaucerType.SMALL:
					if (Target == null)
					{
						_saucerType = SaucerType.BIG;
						return; 
					}
					var dir = Target.position - transform.position;
					dir += new Vector3(Random.Range(-1.0f, 1.0f), Random.Range(-1.0f, 1.0f), Target.position.z);
					float rot_z = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
					Quaternion  q = Quaternion.Euler(0f, 0f, rot_z - 90);
					ObjectPooler.Instance.SpawnFromPool("EnemyBullet", transform.position, q);
					break;
				default:
					break;
			}
			
		}
    }
}