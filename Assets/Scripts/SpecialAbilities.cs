using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace MeatLab
{
	public class SpecialAbilities : MonoBehaviour
	{
		[SerializeField] private GameObject _jet;
		[SerializeField] private Transform[] _jetSpawns;

		[SerializeField] public int TheBoysCount = 1;

		[SerializeField] private GameEvent _calledInTheBoys; 

		public void Update()
		{
			if (Input.GetKeyDown(KeyCode.C))
			{
				if (TheBoysCount > 0)
				{
					TheBoysCount--;
					_calledInTheBoys?.Invoke(); 
				
				}

			}
		}

		public void CallInTheBoys()
		{
			foreach (var item in _jetSpawns)
			{
				Instantiate(_jet, item.position, item.rotation);
			}
		}

		public void GotTheBoys()
		{
			TheBoysCount++;
		}
	}
}