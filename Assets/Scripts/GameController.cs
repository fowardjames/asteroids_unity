using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace MeatLab
{
	[RequireComponent(typeof(RoundController))]
	public class GameController : MonoBehaviour
    {
		public int Score;
		public GameObject CurrentPlayer;
		[Range(3, 5)]
		public int Lives = 3;
		[Range(3, 5)]
		[SerializeField] private int _maxLives = 3;
		[SerializeField] private GameObject _player;
		[SerializeField] private RoundController _roundController;
		[SerializeField] private GameEvent _gameOver;
		[SerializeField] private GameEvent _extraLife;
		private int _extraLifeIndex = 1;

		private void Awake()
		{
			if (_roundController == null)
			{
				_roundController = GetComponent<RoundController>();
			}
		}
		public void StartGame()
		{
			SpawnPlayer();
			_roundController.StartRound(); 
		}

		private void SpawnPlayer()
		{
			GameObject player = Instantiate(_player, Vector2.zero, Quaternion.identity);
			CurrentPlayer = player; 
		}

		public void RemoveLife()
        {
			Lives--;
			if (Lives > 0)
			{
				SpawnPlayer();
				return; 
			}
			_gameOver?.Invoke(); 
		}

		public void RestartGame()
		{
			SceneManager.LoadScene(0); 
		}

		public void UpdateScore(int points)
		{
			Score += points;
			if (((float)Score / (float)10000) > (float)_extraLifeIndex )
			{
				Lives++;
				_extraLife?.Invoke(); 
				_extraLifeIndex++; 
			}
		}
	
    }
}