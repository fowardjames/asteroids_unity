using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace MeatLab
{
    [CreateAssetMenu(fileName = "Data", menuName = "Events/GameEvent", order = 1)]
    public class GameEvent : ScriptableObject
    {
        protected HashSet<GameEventListener> _listeners = new HashSet<GameEventListener>();

        public void Invoke()
        {
            foreach (var item in _listeners)
            {
                item.RaiseEvent();
            }
        }
        public void Register(GameEventListener gameEventListener)
        {
            _listeners.Add(gameEventListener);
        }

        public void Deregister(GameEventListener gameEventListener)
        {
            _listeners.Remove(gameEventListener);
        }
    }
}