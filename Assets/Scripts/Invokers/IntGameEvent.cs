using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MeatLab
{
    [CreateAssetMenu(fileName = "Game Event", menuName = "Events/Int")]
    public class IntGameEvent : GameEvent
    {
        public void Invoke(int value)
        {
            foreach (var item in _listeners)
            {
                item.RaiseEvent(value);
            }
        }
    }
}