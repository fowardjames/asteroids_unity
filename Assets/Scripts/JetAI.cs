using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JetAI : MonoBehaviour
{
    [SerializeField] private BoxCollider2D _collider;

	private void Awake()
	{
		if (_collider == null)
		{
            _collider = GetComponent<BoxCollider2D>();
        }
	}
	public void TurnOffAndOn()
    {
        StartCoroutine(TurnOnCollider());
    }

    public IEnumerator TurnOnCollider()
    {
        _collider.enabled = false;
        yield return new WaitForSeconds(0.25f);
        _collider.enabled = true; 
    }

}
