using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace MeatLab
{
	public interface IPooledObject
	{
		void OnObjectSpawn();
		void OnObjectDeSpawn();
		void OnAudioSpawn(AudioClip audio);
		void OnParticleSystemSpawn();
	}
}