using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace MeatLab
{
    public class PooledGameObject : MonoBehaviour, IPooledObject
    {
        void Awake()
        {
            DontDestroyOnLoad(this);
        }
        public void OnAudioSpawn(AudioClip audio)
        {
            //throw new System.NotImplementedException();
        }

        public void OnObjectSpawn()
        {
            gameObject.SetActive(true);
            //throw new System.NotImplementedException();
        }

        public void OnParticleSystemSpawn()
        {
            //throw new System.NotImplementedException();
        }

        public void OnObjectDeSpawn()
        {
            gameObject.transform.rotation = Quaternion.identity;
            gameObject.SetActive(false);
        }

        public void OnDisable()
        {
            OnObjectDeSpawn();

        }
    }
}