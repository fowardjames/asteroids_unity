using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
namespace MeatLab
{
	public class ParticleSystemPoolable : MonoBehaviour, IPooledObject
	{
		private List<ParticleSystem> ps;
		void Awake()
		{
			DontDestroyOnLoad(this);
			ps = new List<ParticleSystem>();
			ps = GetComponentsInChildren<ParticleSystem>().ToList();
			if (GetComponent<ParticleSystem>())
			{
				ps.Add(GetComponent<ParticleSystem>());
			}
		}
		//public AudioSource 
		public void OnObjectSpawn()
		{
			//Debug.LogError("Add Functionality");
		}
		public void OnAudioSpawn(AudioClip audioClip)
		{

			//Debug.LogError("Add Functionality");
		}


		public void OnParticleSystemSpawn()
		{
			for (int i = 0; i < ps.Count; i++)
			{
				ps[i].Clear();
				ps[i].Play();
			}

		}

		public void OnObjectDeSpawn()
		{
			throw new System.NotImplementedException();
		}
	}
}