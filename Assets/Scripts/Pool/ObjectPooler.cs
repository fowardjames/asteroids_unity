using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace MeatLab
{
	public class ObjectPooler : MonoBehaviour
	{
		private static ObjectPooler _instance;
		public static ObjectPooler Instance { get { return _instance; } }

		[System.Serializable]
		public class Pool
		{
			public string tag;
			public GameObject prefab;
			public int Size;
		}

		public Dictionary<string, Queue<GameObject>> poolDictionary;

		public List<Pool> Pools = new List<Pool>();
		// Start is called before the first frame update
		void Awake()
		{
			if (_instance != null && _instance != this)
			{
				Destroy(this.gameObject);
			}
			else
			{
				_instance = this;
			}
			DontDestroyOnLoad(this);
			poolDictionary = new Dictionary<string, Queue<GameObject>>();

			foreach (Pool pool in Pools)
			{
				Queue<GameObject> objectPool = new Queue<GameObject>();

				for (int i = 0; i < pool.Size; i++)
				{
					GameObject obj = Instantiate(pool.prefab);
					obj.SetActive(false);
					objectPool.Enqueue(obj);
				}
				poolDictionary.Add(pool.tag, objectPool);
			}

		}

		public GameObject SpawnFromPool(string tag, Vector3 position, Quaternion rotation)
		{

			if (!poolDictionary.ContainsKey(tag))
			{
				Debug.LogWarning("Pool With Tag " + tag + " Doesnt Exist");
				return null;
			}

			GameObject objectToSpawn = poolDictionary[tag].Dequeue();

			objectToSpawn.SetActive(true);
			objectToSpawn.transform.position = position;
			objectToSpawn.transform.rotation = rotation;

			IPooledObject poolObj = objectToSpawn.GetComponent<IPooledObject>();
			if (poolObj != null)
			{
				poolObj.OnObjectSpawn();
			}

			poolDictionary[tag].Enqueue(objectToSpawn);

			return objectToSpawn;
		}
		public GameObject SpawnParticleSystemFromPool(string tag, Vector3 position, Quaternion rotation)
		{

			if (!poolDictionary.ContainsKey(tag))
			{
				Debug.LogWarning("Pool With Tag " + tag + " Doesnt Exist");
				return null;
			}

			GameObject objectToSpawn = poolDictionary[tag].Dequeue();

			objectToSpawn.SetActive(true);
			objectToSpawn.transform.position = position;
			objectToSpawn.transform.rotation = rotation;

			IPooledObject poolObj = objectToSpawn.GetComponent<IPooledObject>();
			if (poolObj != null)
			{
				poolObj.OnParticleSystemSpawn();
			}

			poolDictionary[tag].Enqueue(objectToSpawn);

			return objectToSpawn;
		}
		public GameObject SpawnFromAudioPool(string tag, Vector3 position, AudioClip audioClip)
		{

			if (!poolDictionary.ContainsKey(tag))
			{
				Debug.LogWarning("Pool With Tag " + tag + " Doesnt Exist");
				return null;
			}

			GameObject objectToSpawn = poolDictionary[tag].Dequeue();

			objectToSpawn.SetActive(true);
			objectToSpawn.transform.position = position;

			IPooledObject poolObj = objectToSpawn.GetComponent<IPooledObject>();
			if (poolObj != null)
			{
				poolObj.OnAudioSpawn(audioClip);
			}

			poolDictionary[tag].Enqueue(objectToSpawn);

			return objectToSpawn;
		}
		public void PlayRandomSoundFromList(List<AudioClip> list, string poolFamily)
		{
			int rng = Random.Range(0, list.Count);
			ObjectPooler.Instance.SpawnFromAudioPool(poolFamily, Vector3.zero, list[rng]);
		}
	}
}