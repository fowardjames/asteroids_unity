using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MeatLab
{
	public class GameEventOnTrigger : MonoBehaviour
	{
		[SerializeField] private GameEvent _gameEvent;

		private void OnTriggerEnter2D(Collider2D collision)
		{
			if (collision.CompareTag("Player"))
			{
				_gameEvent?.Invoke();
				Destroy(this.gameObject);
			}
			
		}
	}
}
