using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace MeatLab
{
    [RequireComponent(typeof(GameController))]
    public class RoundController : MonoBehaviour
    {
        public int CurrentRound = 1;


        [SerializeField] private GameObject _asteroid;

        [SerializeField] private float _saucerSpawnRate = 15.0f; 
        [SerializeField] private GameObject _bigSaucer;
        [SerializeField] private GameObject _smallSaucer;
        [SerializeField] private MapLimits _mapLimits;
        [SerializeField] private int _enemiesRemaining;
        [SerializeField] private GameController _gameController;
        [SerializeField] private GameObject[] _pickups;

    
        private int _startAsteroidCount = 3;
        private GameObject _currentSaucer;

        Coroutine _spawnSaucers;
        Coroutine _spawnBoxes;

        private void Awake()
		{
			if (_gameController == null)
			{
                _gameController = GetComponent<GameController>();
            }
		}

		public void StartRound()
        {
            _enemiesRemaining = (_startAsteroidCount + CurrentRound) * 7; 
            SpawnBigAsteroids();
			if (_spawnSaucers != null)
			{
                StopCoroutine(_spawnSaucers);
            }
			if (_spawnBoxes != null)
			{
                StopCoroutine(_spawnBoxes);
            }
            _spawnSaucers = StartCoroutine(WaitToSpawnSaucer());
            _spawnBoxes = StartCoroutine(SpawnLootBox());
        }

        private void SpawnBigAsteroids()
        {
            for (int i = 0; i < _startAsteroidCount + CurrentRound; i++)
            {
                Vector2 spawnPoint = new Vector2(_mapLimits.RandomX(), _mapLimits.RandomY());
    
                while (Vector2.Distance(_gameController.CurrentPlayer.transform.position, spawnPoint) < 3)
                {
                    spawnPoint = new Vector2(_mapLimits.RandomX(), _mapLimits.RandomY());
                }

                Instantiate(_asteroid, spawnPoint, Quaternion.identity);
            }
        }

        public void EnemyDead()
        {
            _enemiesRemaining--;
            CheckRoundState();
        }
        public void CheckRoundState()
        {
			if (_enemiesRemaining <= 0)
			{
                CurrentRound++;
                StartRound();
            }
        }

        private IEnumerator WaitToSpawnSaucer()
        {
            yield return new WaitForSeconds(_saucerSpawnRate);
			if (_currentSaucer == null)
			{
                Vector2 spawnPoint = new Vector2(_mapLimits.XBounds, Random.Range(-_mapLimits.YBounds, _mapLimits.YBounds));
                _enemiesRemaining++;
                if (_gameController.Score < 10000)
                {
                    _currentSaucer = Instantiate(_bigSaucer, spawnPoint, Quaternion.identity);
                }
                else if (_gameController.Score >= 10000 && _gameController.Score <= 40000)
                {
                    int rng = Random.Range(0, 2);
                    GameObject saucer = rng == 0 ? _bigSaucer : _smallSaucer;
                    _currentSaucer = Instantiate(saucer, spawnPoint, Quaternion.identity);
                }
                else
                {
                    _currentSaucer = Instantiate(_smallSaucer, spawnPoint, Quaternion.identity);
                }
                _currentSaucer.GetComponent<Saucer>().Target = _gameController.CurrentPlayer.transform; 
            }
            _spawnSaucers = StartCoroutine(WaitToSpawnSaucer());
        }

        private IEnumerator SpawnLootBox()
        {
            yield return new WaitForSeconds(5);
            int rng = Random.Range(0, _pickups.Length);
            Vector2 spawnPoint = new Vector2(Random.Range(-_mapLimits.XBounds, _mapLimits.XBounds), _mapLimits.YBounds + 1);
            Instantiate(_pickups[rng], spawnPoint, Quaternion.identity);
            yield return new WaitForSeconds(20);
            _spawnBoxes = StartCoroutine(SpawnLootBox());
        }
    }
}