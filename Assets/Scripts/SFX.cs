using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace MeatLab
{
	public class SFX : MonoBehaviour, IPooledObject
	{
		void Awake()
		{
			DontDestroyOnLoad(this);
		}
		public void OnObjectSpawn()
		{
			Debug.LogError("Add Functionality");
		}
		public void OnAudioSpawn(AudioClip audioClip)
		{
			AudioSource audioSource = GetComponent<AudioSource>();
			audioSource.clip = audioClip;
			audioSource.Play();

		}

		public void OnParticleSystemSpawn()
		{
			throw new System.NotImplementedException();
		}

		public void OnObjectDeSpawn()
		{
			throw new System.NotImplementedException();
		}
	}
}