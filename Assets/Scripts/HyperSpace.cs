using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace MeatLab
{
	public class HyperSpace : MonoBehaviour
	{
		[SerializeField] private float _hyperSpaceMiniumDistance;

		[SerializeField] private MapLimits _mapLimits;
		private void Update()
		{
			TriggerHyperSpace();
		}
		private void TriggerHyperSpace()
		{
			if (Input.GetKeyDown(KeyCode.H))
			{
				transform.position = ReturnRandomVector();
			}
		}

		private Vector2 ReturnRandomVector()
		{
			Vector2 potentialVector = new Vector2(Random.Range(-_mapLimits.XBounds + 1, _mapLimits.XBounds - 1), Random.Range(-_mapLimits.YBounds + 1, _mapLimits.YBounds - 1));
			while (Vector2.Distance(potentialVector, transform.position) < _hyperSpaceMiniumDistance)
			{
				potentialVector = new Vector2(Random.Range(-_mapLimits.XBounds + 1, _mapLimits.XBounds - 1), Random.Range(-_mapLimits.YBounds + 1, _mapLimits.YBounds - 1));
			}
			return potentialVector;
		}
	}
}