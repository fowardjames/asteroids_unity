using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace MeatLab
{
    public class PlayerMovement : MonoBehaviour
    {
        [SerializeField] private Rigidbody2D _rb;

        [SerializeField] private float _thrust;

        [SerializeField] private float _rotationSpeed;

		[SerializeField] private float _maxSpeed = 10;

		[SerializeField] private AudioSource _audioSource;

		private void FixedUpdate()
		{
			ForwardThrust();
			Turn();
		}

		private void Turn()
		{
			if (Input.GetAxisRaw("Horizontal") < 0)
			{
				transform.Rotate(Vector3.forward * _rotationSpeed);
			}
			if (Input.GetAxisRaw("Horizontal") > 0)
			{
				transform.Rotate(Vector3.forward * -_rotationSpeed);
			}
		}

		private void ForwardThrust()
		{
			if (Input.GetAxisRaw("Vertical") > 0)
			{
				_rb.AddForce(transform.up * _thrust * Time.fixedDeltaTime, ForceMode2D.Impulse);

				_rb.velocity = Vector3.ClampMagnitude(_rb.velocity, _maxSpeed);

				_audioSource.volume = 1; 
			}
			else
			{
				_audioSource.volume = 0; 
			}
		}
	}
}

