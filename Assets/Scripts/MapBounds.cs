using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace MeatLab
{
	public class MapBounds : MonoBehaviour
	{

		[SerializeField] protected MapLimits _mapLimits;

		// Update is called once per frame
		void Update()
		{
			CheckBoundsAndCorrectPlayer();
		}

		protected virtual void CheckBoundsAndCorrectPlayer()
		{
			if (transform.position.x < -_mapLimits.XBounds)
			{
				transform.position = new Vector3(_mapLimits.XBounds, transform.position.y, transform.position.z);
			}
			if (transform.position.x > _mapLimits.XBounds)
			{
				transform.position = new Vector3(-_mapLimits.XBounds, transform.position.y, transform.position.z);
			}

			if (transform.position.y < -_mapLimits.YBounds)
			{
				transform.position = new Vector3(transform.position.x, _mapLimits.YBounds, transform.position.z);
			}
			if (transform.position.y > _mapLimits.YBounds)
			{
				transform.position = new Vector3(transform.position.x, -_mapLimits.YBounds, transform.position.z);
			}
		}


	}
}