using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
namespace MeatLab
{
    public class IntEventListener : GameEventListener
    {
        public IntGameEvent IntResponse;
        [SerializeField] private UnityEvent<int> _unityEventInt;
        public override void RaiseEvent(int value)
        {

            _unityEventInt.Invoke(value);
        }
    }
}