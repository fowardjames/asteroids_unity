using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
namespace MeatLab
{
	public class GameEventListener : MonoBehaviour
	{
		[SerializeField] protected GameEvent _gameEvent;
		[SerializeField] protected UnityEvent _unityEvent;
		// Start is called before the first frame update
		private void Awake()
		{
			_gameEvent.Register(this);
		}

		private void OnDestroy()
		{
			_gameEvent.Deregister(this);
		}

		public virtual void RaiseEvent()
		{
			_unityEvent.Invoke();
		}

		public virtual void RaiseEvent(int value)
		{

		}

		public virtual void RaiseEvent(AudioClip value)
		{

		}
	}
}