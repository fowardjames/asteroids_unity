using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace MeatLab
{
    [CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/MapLimits", order = 1)]
    public class MapLimits : ScriptableObject
    {
        [SerializeField] public float XBounds;
        [SerializeField] public float YBounds;

        public float RandomX()
        {
            return Random.Range(-XBounds, XBounds);
        }
        public float RandomY()
        {
            return Random.Range(-YBounds, YBounds); 
        }
    }
}