using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace MeatLab
{
    public class MapBoundsPlayer : MapBounds
    {
		[SerializeField] private TrailRenderer _trail; 
		protected override void CheckBoundsAndCorrectPlayer()
		{
			if (transform.position.x < -_mapLimits.XBounds)
			{
				transform.position = new Vector3(_mapLimits.XBounds, transform.position.y, transform.position.z);
				_trail.Clear(); 
			}
			if (transform.position.x > _mapLimits.XBounds)
			{
				transform.position = new Vector3(-_mapLimits.XBounds, transform.position.y, transform.position.z);
				_trail.Clear();
			}

			if (transform.position.y < -_mapLimits.YBounds)
			{
				transform.position = new Vector3(transform.position.x, _mapLimits.YBounds, transform.position.z);
				_trail.Clear();
			}
			if (transform.position.y > _mapLimits.YBounds)
			{
				transform.position = new Vector3(transform.position.x, -_mapLimits.YBounds, transform.position.z);
				_trail.Clear();
			}
		}
	}
}