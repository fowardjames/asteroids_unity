using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace MeatLab
{
    public class PlayerShoot : MonoBehaviour
    {
        [SerializeField] private int _currentBulletCount;
        [SerializeField] private int _bulletMax;

        [SerializeField] private float _reloadTime = 0.3f;

        private GameObject _currentBullet;

        [SerializeField] private Transform _firePoint;

        [SerializeField] private Transform[] _firePointSpray;

        [SerializeField] private Transform[] _firePointTriple;


        [SerializeField] private AudioClip _shotSFX;

        enum WeaponType
        {
            Basic, 
            Burst, 
            Spray
        }

        [SerializeField] private WeaponType _currentWeapon; 
        


        // Update is called once per frame
        void Update()
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
				switch (_currentWeapon)
				{
					case WeaponType.Basic:
                        ShootBasic();
                        break;
					case WeaponType.Burst:
                        ShootTriple(_firePointTriple);
                        break;
					case WeaponType.Spray:
                        ShootTriple(_firePointSpray);

                        break;
					default:
						break;
				}
				
            }
        }

        private void ShootBasic()
        {
            if (_currentBulletCount <= 0) return;

            _currentBulletCount--;
            ObjectPooler.Instance.SpawnFromAudioPool("SFX", Vector3.zero, _shotSFX);
            ObjectPooler.Instance.SpawnFromPool("Bullet", _firePoint.position, _firePoint.rotation);
            if (_currentBulletCount <= 0)
            {
                StartCoroutine(WaitToReload());
            }
        }

        private void ShootTriple(Transform[] firePoints)
        {
            if (_currentBulletCount <= 0) return;

            _currentBulletCount--;

			for (int i = 0; i < _firePointSpray.Length; i++)
			{
                ObjectPooler.Instance.SpawnFromPool("Bullet", firePoints[i].position, firePoints[i].rotation);
            }
            ObjectPooler.Instance.SpawnFromAudioPool("SFX", Vector3.zero, _shotSFX);
           
            if (_currentBulletCount <= 0)
            {
                StartCoroutine(WaitToReload());
            }
        }

        private IEnumerator WaitToReload()
        {

            yield return new WaitForSeconds(_reloadTime);
            _currentBulletCount = _bulletMax;

        }

        public void RandomWeapon()
        {
            int rng = Random.Range(0,2);

            _currentWeapon = rng == 0 ? WeaponType.Burst : WeaponType.Spray;
        }
    }

    
}