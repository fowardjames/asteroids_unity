using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace MeatLab
{
	public class PlayerHealth : MonoBehaviour
	{
		[SerializeField] private GameEvent _removeLife;

		[SerializeField] private EdgeCollider2D _collider;

		[Header("How long the player is invulrable when spawning")]
		[SerializeField] private float _godModeTimer = 1.0f;

		[SerializeField] private AudioClip _deathSFX; 

		private void OnEnable()
		{
			if (_collider == null)
			{
				GetComponent<EdgeCollider2D>();
			}
			StartCoroutine(WaitToTurnOnCollider());
		}

		public IEnumerator WaitToTurnOnCollider()
		{
			yield return new WaitForSeconds(_godModeTimer);
			_collider.enabled = true;
		}

		private void OnTriggerEnter2D(Collider2D other)
		{
			if (other.CompareTag("Pickup"))
			{
				return; 
			}
			ObjectPooler.Instance.SpawnFromAudioPool("SFX", Vector3.zero, _deathSFX);
			_removeLife?.Invoke();
			Destroy(this.gameObject);
		}
	}
}